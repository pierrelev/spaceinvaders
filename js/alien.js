class Alien{
	constructor(x,y) {
        let parent=document.getElementById("gameboard");
		this.x=x;
		this.y=y;
		this.speed=1;
		this.width=0;
		this.height=0;
		//Création de l'élément ici
		this.img = document.createElement("div");
		this.img.setAttribute("id", "alienDOM");

		//tiledImage = new TiledImage("../images/aliens.png", 4, 2, 100, true, 0.05, "alienDOM");

		this.img.style.left=x+"px";
		this.img.style.top=y+"px";
		this.img.style.position="absolute";

		parent.appendChild(this.img);
	}

	tick(){
		this.width=document.getElementById("alienDOM").offsetWidth;
        this.height=document.getElementById("alienDOM").offsetHeight;
		
		if(this.x>0)
		{
			this.x=this.x-this.speed;
			//tiledImage.tick(this.x, this.y);
			//tiledImage1.tick(this.x, this.y);
			this.img.style.left=this.x+"px";
		}
		else
		{
			this.img.style.display="none";
			let s = spaceshipList[0];
			s.lives=s.lives-1;
		}
		
	}

	disappear(){
		this.img.style.display="none";
	}

	
}
