class Fire{
    constructor(x,y) {
    let parent=document.getElementById("gameboard");

    this.x=x;
    this.y=y;
    this.speed=6;

    //Création de l'élément ici
    this.fire = document.createElement("div");
    this.fire.setAttribute("id", "fireDOM");

    this.fire.style.left=x+"px";
    this.fire.style.top=y+"px";
    this.fire.style.backgroundColor="red";
    this.fire.style.height="7px";
    this.fire.style.width="7px";
    this.fire.style.position="absolute";
    this.fire.style.borderRadius="5px";

    parent.appendChild(this.fire);
}

    tick(){
        this.x=this.x+this.speed;
        this.fire.style.left=this.x+"px";

    }

    disappear(){
		this.fire.style.display="none";
    }

}