//Disable le spacebar comme outil de scroll down

window.onkeydown=function(e){
	if(e.keyCode==32){
	 return false;
	}
  };

// =======================================================
// Animation in a canvas
// -------------------------------------------------------
//https://apps-de-cours.com/utils/sprite-sheet-creator/

//Alien
let columnCountA = 4; 
let rowCountA = 2;

//Spaceship
let columnCountS = 2; 
let rowCountS = 3;
let rowS=0;
let timer=0;
let score=0;


let refreshDelay = 100; 			// msec
let loopColumns = true; 			// or by row?
let scaleS = 1.0;
let scaleA = 0.05;
let ctx = null;
let alienList=[];
let spaceshipList=[];
let fireList=[];

var rightPushed = false;
var leftPushed = false;
var upPushed = false;
var downPushed = false;
var spacePushed = false;

var run=true;

let tiledImage = null; 
let tiledImageHeros = null; 

//tiledImage.changeMinMaxInterval(1, 9); 	// Loop from which column to which column?


// You can stack images!
//tiledImage.addImage("images/item-hood-walk.png");
//tiledImage.addImage("images/item-shield-walk.png");

// Other functions :
//tiledImageHeros.setFlipped(true);		// Horizontally
//tiledImage.setLooped(false);

//Pour l'animation du sprite
window.onload = () => {
	
	var x=960;

	// Première création d'aliens
	createAliens();

	//Crée le spaceship
	spaceshipList.push(new Spaceship(100,200));
	//tiledImage = new TiledImage("../images/aliens.png", columnCountA, rowCountA, refreshDelay, loopColumns, scaleA, "alienDOM");
	setInfos();



	tiledImageHeros =  new TiledImage("../images/spaceship1.png", columnCountS, rowCountS, refreshDelay, loopColumns, scaleS, "spaceshipDOM");

	//tiledImage.changeMinMaxInterval(1,4);
	tiledImageHeros.changeMinMaxInterval(0, 2);
	//tiledImageHeros.changeRow(1);				// One row per animation
	//tiledImageHeros.setLooped(false);

	intervalChangeRow();

	tick();


function tick() {
	setInfos();
	mortAlien();
	collisionSpaceship();
	const spaceship = spaceshipList[0];

	spaceship.tick(40,20);
	
	//Affichage d'aliens
	
	if(timer==190){
		createAliens();
	}

	if(alienList.length!=0){
		for(var i=(alienList.length-1);i>=0; i--){
			let alien = alienList[i];
			alien.tick();
		} 	
	}

	//Affichage de balles
	if(fireList[0]!=null){
		for(var i=0;i<fireList.length; i++){
			let fire = fireList[i];
			fire.tick();
		} 	
	}
	collisionSpaceship();
	mortAlien();
	rightLimit();
	leftLimit();
	setInfos();

	timer++;

	if(timer==200)
	{
		timer=0;
	}
		
	if(run==true)
	{
		window.requestAnimationFrame(tick);
	}
	
}

function setInfos(){

	//Lives
	document.getElementById("lives").textContent=spaceshipList[0].lives+" lives";

	//Score
	document.getElementById("score").textContent=score+" points";

}

//Pour l'animation du spaceship
function intervalChangeRow(){
	setInterval(function(){ 
		rowS++;
		if(rowS==3)
		{
			rowS=0;
		}
	}, 1000);
}

//Pour la vérification si un alien est mort
function mortAlien() 
{
	for(let i=0; i<alienList.length;i++)
	{
		try{
			for(let j=0;j<fireList.length;j++)
			{
				
				let f = fireList[j];
				let a = alienList[i];
				if(f.x>=a.x && f.y>=a.y && f.y<=(a.y+30))
				{
					fireList[j].disappear();
					alienList[i].disappear();
	
					alienList.splice(i,1);
					fireList.splice(j,1);
					score=score+1;
				}
			}
		}
		catch(error){

		}
	
	}
}

function createAliens(){
	for(let i=0;i<5;i++) {
		if(i==0){
			var yRandom = Math.floor((Math.random() * 250)+ 1)+50;
			alienList.push(new Alien(x,yRandom));
		}
		else{
			yRandom=yRandom+40;
			alienList.push(new Alien(x,yRandom));
		}
			
	}
}

function rightLimit(){
	for(let j=0;j<fireList.length;j++)
		{
			try{
				let f = fireList[j];
				if(f.x>1000)
				{
					fireList[j].disappear();
					fireList.splice(j,1);
				}
			}
			catch(err){
			}
		}
}


//Pour la vérification si le spaceship perd une vie
function collisionSpaceship(){

	for(let i=0; i<alienList.length;i++)
	{

		let s = spaceshipList[0];
		let a = alienList[i];
		let downLimitS=s.y+s.height;
		let downLimitA=a.y+a.height;
		let rightLimitS=s.x+s.width;
		let rightLimitA=a.x+a.width;
		//Si le spaceship est entre les limites de l'alien
		if(rightLimitS>=a.x && s.x<=rightLimitA && s.y<=downLimitA && downLimitS>=a.y)
		{
			alienList[i].disappear();
			alienList.splice(i,1);
			lostLife();
		}
		
		
	}
}


function lostLife() {
	let s= spaceshipList[0];
	s.lives=s.lives-1;
	if(s.lives<=0)
	{
		gameOver();
	}
}

function leftLimit() {
	for(let i=0; i<alienList.length;i++)
	{
		let a=alienList[i];
		if(a.x<=0) {
			let s = spaceshipList[0];
			s.lives=s.lives-1;
			alienList.splice(i,1);
			a.disappear();
			lostLife();
		}
	}
}

//Game over!
function gameOver(){
	var parent=document.getElementById("gameboard");
	var gameover = document.createElement("div");

	gameover.setAttribute("id", "gameover");
	parent.appendChild(gameover);
	document.getElementById("gameover").textContent="GAME OVER";

	run=false;


}




//Pour le keyboard
document.onkeydown = function (e) {
    if (e.which == 65)         leftPushed = true;
    else if (e.which == 68) rightPushed = true;
	else if (e.which == 87) upPushed = true;
	else if (e.which == 83) downPushed = true;

	if (e.which == 32) spacePushed = true;
	
}

document.onkeyup = function (e) {
	if (e.which == 65)         leftPushed = false;
    else if (e.which == 68) rightPushed = false;
	else if (e.which == 87) upPushed = false;
	else if (e.which == 83) downPushed = false;

	if (e.which == 32) spacePushed = false;
	
}
}


